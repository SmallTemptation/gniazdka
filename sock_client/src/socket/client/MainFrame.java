package socket.client;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.MaskFormatter;
import socket.client.buttons.ShipButton;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JFormattedTextField;
import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.border.LineBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MainFrame extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 430225183310129755L;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * 
	 * @throws Exception
	 */
	public MainFrame() throws Exception {

		Player player = new Player();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 650, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblHost = new JLabel("Host:");
		lblHost.setBounds(10, 14, 33, 14);
		contentPane.add(lblHost);

		JFormattedTextField addressIpField;
		MaskFormatter maskIp = new MaskFormatter("***.***.*.*");
		addressIpField = new JFormattedTextField(maskIp);
		addressIpField.setBounds(44, 11, 89, 20);
		// frmtdtxtfldIp.setText("xxx.xxx.x.x");
		maskIp.setPlaceholderCharacter('_');
		addressIpField.setText("127.000.0.1");
		contentPane.add(addressIpField);

		JLabel labelPort = new JLabel("Port:");
		labelPort.setBounds(10, 42, 33, 14);
		contentPane.add(labelPort);

		JFormattedTextField portField;
		MaskFormatter maskPort = new MaskFormatter("####*");
		// portField.setText("xxxx");
		portField = new JFormattedTextField(maskPort);
		maskPort.setPlaceholderCharacter('_');
		portField.setBounds(44, 39, 89, 20);
		portField.setText("60666");
		contentPane.add(portField);

		JPanel panel = new JPanel();
		panel.setBackground(new Color(230, 255, 255));
		panel.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		panel.setBounds(140, 11, 484, 439);
		panel.setLayout(new GridLayout(5, 5, 5, 5));

		for (int i = 0; i < 25; i++) {
			ShipButton opponentButton = new ShipButton(i);
			// opponentButton.addActionListener(new ShipButtonListener());
			opponentButton.setText("");
			add(opponentButton);
			opponentButton.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					try {
						int userChoise = opponentButton.getPosition(); // opisanie dzia�ania klikni�cia w pole przyciwnika																	
						player.sendOrderPanePossition(userChoise); // przes�anie pozycji klikni�cia do serwera
						if (player.getStateOfHit()) {
							setBackground(new Color(250, 29, 29));
						} else
							setBackground(new Color(51, 153, 255));
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			panel.add(opponentButton);
		}
		contentPane.add(panel);

		JLabel labelConnected = new JLabel("Connected");
		labelConnected.setForeground(new Color(0, 204, 0));
		labelConnected.setOpaque(true);
		labelConnected.setBounds(10, 104, 123, 23);

		JLabel labelNotConnected = new JLabel("Not Connected");
		labelNotConnected.setForeground(new Color(204, 0, 0));
		labelNotConnected.setOpaque(true);
		labelNotConnected.setBounds(10, 104, 123, 23);

		// Creating panel
		JPanel playerBoard = new JPanel();
		playerBoard.setForeground(Color.GRAY);
		playerBoard.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		playerBoard.setBounds(10, 138, 123, 123);
		contentPane.add(playerBoard);
		playerBoard.setLayout(new GridLayout(5, 5, 5, 5));
		UserBoardState userBoard = new UserBoardState();

		for (int i = 0; i < 25; i++) {
			ShipButton userButton = new ShipButton(i); // zapisuj� pozycj�, �eby wiedzie�, kt�ry button naci�ni�to
			userButton.addActionListener(new UserButtonListener());
			userButton.setBackground(new Color(230, 255, 255));
			playerBoard.add(userButton);

			userButton.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					try {
						if (UserBoardState.clientBoardState[userButton.getPosition()] == 0) {
							if (userBoard.countShips() < 3) {
								userBoard.setClientBoardState(userButton.getPosition(), 1);
								userButton.setBackground(Color.RED);
							}
						} else if (UserBoardState.clientBoardState[userButton.getPosition()] == 1) {
							userBoard.setClientBoardState(userButton.getPosition(), 0);
							userButton.setBackground(new Color(230, 255, 255));
						}
						// System.out.print(Arrays.toString(userBoard.clientBoardState));
						// System.out.println(userBoard.countShips());
						// } else {
						// userButton.setEnabled(false);
						// }

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
		}

		JButton btnConnect = new JButton("Connect");
		btnConnect.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				try {
					String serverHostIp = addressIpField.getText();
					String serverPort = portField.getText();
					if (serverHostIp.trim().length() > 0 && serverPort.trim().length() > 0) {
						player.setConnection(serverHostIp, Integer.parseInt(serverPort));
						contentPane.add(labelConnected);
						contentPane.repaint();
					} else {
						contentPane.add(labelNotConnected);
						contentPane.repaint();
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnConnect.setBounds(20, 70, 95, 23);
		contentPane.add(btnConnect);

		JButton btnSendBoard = new JButton("Send Board");
		btnSendBoard.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				try {
					/**
					 * zliczanie statk�w i wys�ylanie tylko gdy b�dzie ich 9, dodanie labels -
					 * komunikacja z u�ytkownikiem
					 **/
					// jak zap�tli� to do momentu uzyskania 9 statk�w z mo�liwo�ci zmiany 9 statku i
					// aktualizacj� labela po naci�ni�ciu sendBoard

					// JLabel labelChooseMoreShips;
					// int missingShips = 9 - userBoard.countShips();
					//
					// while (missingShips != 0) {
					//
					// if (missingShips == 1) {
					// labelChooseMoreShips = new JLabel("Choose 1 more ship.");
					// labelChooseMoreShips.setForeground(new Color(204, 0, 0));
					// labelChooseMoreShips.setOpaque(true);
					// labelChooseMoreShips.setBounds(10, 301, 123, 23);
					// contentPane.add(labelChooseMoreShips);
					// contentPane.repaint();
					// } else {
					// labelChooseMoreShips = new JLabel("Choose " + missingShips + " more ships.");
					// labelChooseMoreShips.setForeground(new Color(204, 0, 0));
					// labelChooseMoreShips.setOpaque(true);
					// labelChooseMoreShips.setBounds(10, 301, 123, 23);
					// contentPane.add(labelChooseMoreShips);
					// contentPane.repaint();
					// }
					//
					// }
					player.sendPlayersBoardState();
					player.play();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnSendBoard.setBounds(20, 281, 95, 23);
		contentPane.add(btnSendBoard);
	}

	class UserButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			// arg0.
		}

	}
}