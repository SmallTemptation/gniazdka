package socket.client.buttons;

import javax.swing.JButton;

@SuppressWarnings("serial")

public class ShipButton extends JButton {
	protected int position;
	protected int type; // 0-sea, 1-ship

	public ShipButton(int position) {
		this.position = position;
		this.type = 0;
	}
	
	public int getPosition() {
		return this.position;
	}
	
	public int getType() {
		return this.type;
	}
	
	public void setType(int type) {
		this.type = type;
	}
}
