package socket.client;

import java.io.BufferedReader;
//import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Player extends Thread {
	protected int len = 25;
	private int serverPort;
	private String serverIP;
	// DataInputStream input;
	BufferedReader is;
	PrintWriter output;
	Socket playerSocket;
	boolean hit;
	boolean win;

	public void setConnection(String serverIP, int serverPort) throws Exception {
		this.serverPort = serverPort;
		this.serverIP = serverIP;
		System.out.println("Connection set");

		this.playerSocket = new Socket(this.serverIP, this.serverPort);
		// this.input = new DataInputStream(playerSocket.getInputStream());
		this.is = new BufferedReader(new InputStreamReader(playerSocket.getInputStream()));
		this.output = new PrintWriter(playerSocket.getOutputStream(), true);
		System.out.println("Connection established");
	}

	public void sendPlayersBoardState() throws Exception {
		System.out.println("sendBoardState start");
		String command = "BOARD";
		for (int j = 0; j < len; j++) {
			command += UserBoardState.clientBoardState[j] + ",";
		}
		command = command.substring(0, command.length() - 1); // usuni�cie przecinka na koncu
		output.println(command);
		System.out.println(command);
		System.out.println("Board state sent to server");
	}

	public boolean getStateOfHit() {
		return hit;
	}

	private boolean wantsToPlayAgain() {
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(240, 160);
		frame.setVisible(true);
		frame.setResizable(false);
		String message = "Do you wan to play again?";
		int answer = JOptionPane.showConfirmDialog(frame, message, "Ship Battle!", JOptionPane.YES_NO_OPTION);
		frame.dispose();
		return (answer == JOptionPane.YES_OPTION);
	}

	public void sendOrderPanePossition(int position) throws Exception {
		output.println("HIT" + position);
	}

	public void comunicateServer() {
		this.run();
	}

	public void run() {
		System.out.println("MyThread running");
		String response;

		try {
			while (true) {
				System.out.println("Comunicate while 1");
				response = is.readLine(); // tu si� zatrzymuje i czeka wieczno�� na odpowiedz serwera

				System.out.println("comunicate 5");
				if (response.startsWith("SUCCESS")) {
					hit = true;
				} else if (response.startsWith("MISHIT")) {
					hit = false;
				} else if (response.startsWith("VICTORY")) {
					win = true;
					break;
				} else if (response.startsWith("DEFEAT")) {
					win = false;
					break;
				} else if (response.startsWith("OPONENTDISCONNECTED")) {
					boolean continueGame = this.wantsToPlayAgain();
					System.out.println("Play 3");
					if (!continueGame) {
						break;
					}
				}

				System.out.println("comunicate 6");
			}
			output.println("QUIT");

			System.out.println("comunicate 7");
			playerSocket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void play() throws Exception {
		System.out.println("comunicate 1");
		String response = is.readLine();
		System.out.println("comunicate 2");
		if (response.startsWith("WELCOME")) {
			System.out.println("comunicate 3");
			// sendPlayersBoardState();
		}
		System.out.println("comunicate 4");
		System.out.println("Play 1");
		this.comunicateServer();
		System.out.println("Play 2");
	}
}