package socket.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	// private String sessionNumber;
	// private static int playersCount = 0;

	public static void main(String[] args) throws Exception {
		int playerServerPort = 60666;

		ServerSocket listener = new ServerSocket(playerServerPort);
		System.out.println("Server ready for connections");
		try {
			while (true) {
				Game game = new Game();
				Game.Player Player1 = game.new Player(listener.accept(), 1);
				Game.Player Player2 = game.new Player(listener.accept(), 2);
				game.currentPlayer = Player1;
				Player1.setOponent(Player2);
				Player2.setOponent(Player1);

				Player1.start();
				Player2.start();
			}
		} finally {
			listener.close();
		}
	}
}

class Game {
	private Integer[] playerBoardState = new Integer[25];
	public Player currentPlayer;

	public boolean thePlayerHasHit(int position) {
		if (playerBoardState[position] == 1) {
			playerBoardState[position] = 2;
			return true;
		} else if (playerBoardState[position] == 0)
			return false;
		else if (playerBoardState[position] == 2)
			return false;
		return false;
	}

	public boolean isWinner() {
		for (int i = 0; i < 25; i++) {
			if (playerBoardState[i] == 1)
				return false;
		}
		return true;
	}

	class Player extends Thread {
		Player opponent;
		Socket socket;
		int playerNumer;
		BufferedReader is;
		PrintWriter output;

		public Player(Socket socket, int playerNumer) {
			this.socket = socket;
			this.playerNumer = playerNumer;

			try {
				is = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				output = new PrintWriter(socket.getOutputStream(), true);
				output.println("WELCOME!");
			} catch (IOException e) {
				System.out.println("Player died: " + e);
			}
		}

		public void setOponent(Player opponent) {
			this.opponent = opponent;
		}

		public void run() {
			try {
				System.out.println("Server run 1");
//					output.writeChars("WELCOME! Set board");
				while (true) {
					System.out.println("Server run 2");
					String command = is.readLine();

					System.out.println("Server run 3");
					if (command.startsWith("HIT")) {
						int position = Integer.parseInt(command.substring(3));
						if (thePlayerHasHit(position)) {
							output.println("SUCCESS");
							output.println(isWinner() ? "VICTORY" : "");
						} else {
							output.println("MISHIT");
						}

					} else if (command.startsWith("BOARD")) {
						String temp = command.substring(5);
						String tempA[] = temp.split(",");
						System.out.println(tempA.length);
						int boardLength = playerBoardState.length;
						for (int i = 0; i < boardLength; i++) {
							playerBoardState[i] = Integer.parseInt(tempA[i]);
						}
					} else if (command.startsWith("QUIT")) {
						output.println("OPONENTDISCONNECTED");
						return;
					}
				}
			} catch (IOException e) {
				output.println("OPONENTDISCONNECTED");
				System.out.println("Player died: " + e);
			} finally {
				try {
					socket.close();
				} catch (IOException e) {
				}
			}
		}
	}
}
