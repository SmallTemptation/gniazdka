package socket.client;

import java.util.Arrays;

public class UserBoardState {
	public static Integer[] clientBoardState;

	public UserBoardState() {
		clientBoardState = new Integer[25];
		Arrays.fill(clientBoardState, 0);
	}

	public void setClientBoardState(int position, int newState) {
		int currentState = UserBoardState.clientBoardState[position];
		switch (currentState) {
		case 0:
			UserBoardState.clientBoardState[position] = newState;
			// zapisanie warunk�w na pozycjach obok zaznaczonej
			break;
		case 1:
			UserBoardState.clientBoardState[position] = newState;
			break;
		}
	}

	public static Integer[] getClientBoardState() {
		return clientBoardState;
	}

	public int countShips() {
		int counter = 0;
		for (int i : UserBoardState.clientBoardState) {
			if (i == 1)
				counter += 1;
		}
		return counter;
	}
}